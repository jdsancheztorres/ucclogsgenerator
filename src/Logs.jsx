import React, { useState, useEffect } from 'react';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Container,
  FormControl,
  Field,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { retrievePositions } from './actions/positions';
import { retrieveTopics } from './actions/topics';
import { createUserLogs } from "./actions/userlogs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./style.css";

const Logs = () => {

  const initialNewUserLogsState = {
    name: '',
    lastName: '',
    company: '',
    userPosition: '',
    email: '',
    topicList : []
  };
  const [newUserLogs, setNewUserLogs] = useState(initialNewUserLogsState);
  const dispatch = useDispatch();
  const positions = useSelector(state => state.positions);
  const topics = useSelector(state => state.topics);

  useEffect(() => {
    console.log("Logs::userEffect");
    dispatch(retrievePositions());
    dispatch(retrieveTopics());
  }, []);

  return (
    <>
      <ToastContainer autoClose={5000} position="top-center" /> 
      <Helmet>
        <title>UCC - Generador de logs</title>
      </Helmet>
      
      <Box
        sx={{
          backgroundColor: '#FFFFFF',
          display: 'flex',
          flexDirection: 'column',
          height: '100%',
          justifyContent: 'center'
        }}
      >
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              name: '',
              lastName: '',
              company: '',
              userPosition: '',
              email: '',
              topicList :[]
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().max(255).required('El campo Nombres es requerido'),
              lastName: Yup.string().max(255).required('El campo Apellidos es requerido'),
              company: Yup.string().max(255).required('El campo Empresa es requerido'),
              userPosition: Yup.string().max(255).required('El campo Cargo es requerido'),
              email: Yup.string().max(255).required('El campo Correo electrónico es requerido')
            })}
            handleChange={(e) => {
              const name = e.target.name;
              const value = e.target.value;

              setNewUserLogs({
                ...newUserLogs,
                [name]: value,
              });
            }}
            onSubmit={(values) => {
              newUserLogs.name = values.name;
              newUserLogs.lastName = values.lastName;
              newUserLogs.company = values.company;
              newUserLogs.userPosition = values.userPosition;
              newUserLogs.email = values.email;
              newUserLogs.topics = values.topicList;
              toast.success("Muchas gracias por tu ayuda. Esté pendiente de su correo electrónico.");
              dispatch(createUserLogs(newUserLogs));         
            }}
          >
            {({
              errors,
              handleSubmit,
              handleChange,
              isSubmitting,
              touched,
              values
            }) => (
              <div className="loginBox">
                <form onSubmit={handleSubmit}>
                  <Grid
                    container
                    spacing={3}
                  >
                    <Grid
                      item
                      xs={12}
                      md={6}
                    >

                    </Grid>
                    <Grid
                      item
                      xs={12}
                      md={6}
                    >

                    </Grid>
                  </Grid>
                  <Box
                    sx={{
                      pb: 1,
                      pt: 3
                    }}
                  >
                  </Box>
                  <Box sx={{ mb: 3 }}>
                    <Typography
                      color="textPrimary"
                      variant="h2"
                    >
                      UCC - Generador de logs
                    </Typography>
                    <br></br>
                    <Typography
                      color="textSecondary"
                      gutterBottom
                      variant="body2"
                      align='left'
                    >
                      Diligencia todos los campos del formulario y presione el botón al final.
                    </Typography>
                  </Box>
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Nombres"
                    margin="normal"
                    floatingLabelText="email"
                    name="name"
                    onChange={handleChange}
                    type="text"
                    value={values.name}
                    variant="outlined"
                  />
                  <TextField
                    error={Boolean(touched.lastName && errors.lastName)}
                    fullWidth
                    helperText={touched.lastName && errors.lastName}
                    label="Apellidos"
                    margin="normal"
                    name="lastName"
                    onChange={handleChange}
                    type="text"
                    value={values.lastName}
                    variant="outlined"
                  />
                  <TextField
                    error={Boolean(touched.company && errors.company)}
                    fullWidth
                    helperText={touched.company && errors.company}
                    label="Empresa"
                    margin="normal"
                    name="company"
                    onChange={handleChange}
                    type="text"
                    value={values.company}
                    variant="outlined"
                  />
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Cargo</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      name="userPosition"
                      value={values.userPosition}
                      label="Cargo"
                      onChange={handleChange}
                    >
                      {
                        positions.map((item, index) => {
                          return <MenuItem value={item.id}>{item.name}</MenuItem>
                        })
                      }
                    </Select>
                  </FormControl>
                  <TextField
                    error={Boolean(touched.email && errors.email)}
                    fullWidth
                    helperText={touched.email && errors.email}
                    label="Correo electrónico"
                    margin="normal"
                    name="email"
                    onChange={handleChange}
                    type="email"
                    value={values.email}
                    variant="outlined"
                  />
                  <br></br>
                  <br></br>
                  <Typography
                    color="textSecondary"
                    gutterBottom
                    variant="body2"
                    align='left'
                  >
                    Temas de interés:
                  </Typography>
                  <br></br>
                  <FormControl fullWidth>

                    {topics.map((item, index) => (
                      <div key={index}>
                        <input
                          id={item.id}
                          type="checkbox"
                          value={item.name}
                          name="topicList"
                          onChange={handleChange}
                        />
                        <label htmlFor={item.name}>{item.name}</label>
                      </div>
                    ))}
                  </FormControl>
                  <Box sx={{ py: 2 }}>
                    <Button
                      color="primary"
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                    >
                      Recibir Log
                    </Button>
                  </Box>
                </form>
              </div>
            )}
          </Formik>
        </Container>
      </Box>
    </>
  );
}


export default Logs;
