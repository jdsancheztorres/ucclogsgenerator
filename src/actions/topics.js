import {
    RETRIEVE_TOPICS,
} from "./types";
import TopicService from "../services/TopicService";

export const retrieveTopics = (request) => async (dispatch) => {
    try {
        const res = await TopicService.getAll(request);
        dispatch({
        type: RETRIEVE_TOPICS,
        payload: res,
        });
    } catch (err) {
        console.log(err);
    }
};