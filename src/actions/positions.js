import {
    RETRIEVE_POSITIONS,
} from "./types";
import PositionService from "../services/PositionService";

export const retrievePositions = (request) => async (dispatch) => {
    try {
        const res = await PositionService.getAll(request);
        dispatch({
        type: RETRIEVE_POSITIONS,
        payload: res,
        });
    } catch (err) {
        console.log(err);
    }
};