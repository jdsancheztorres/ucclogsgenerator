import {
    CREATE_USER_LOGS
} from "./types";
import UserLogsService from "../services/UserLogsService";
  
export const createUserLogs = (request) => async (dispatch) => {
    try {
        console.log("UserLogsService::createUserLogs -> ", request);
        const res = await UserLogsService.createUserLogs(request)
        dispatch({
            type: CREATE_USER_LOGS,
            payload: res,
        });
        console.log("UserLogsService::createUserLogs -> res ->> ", res);
    } catch (err) {
        console.log(err);
    } 
};