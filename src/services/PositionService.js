import properties from '../conf/properties';
import handleResponse  from './Base';

const configuration = {
    apiUrl : properties.logsServiceUrl
}

const getAll = () => {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };

    return fetch(`${configuration.apiUrl}/api/position/`, requestOptions).then(handleResponse);
};

const PositionService = {
    getAll,
};

export default PositionService;