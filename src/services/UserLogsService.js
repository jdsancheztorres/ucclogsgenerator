import properties from '../conf/properties';
import handleResponse  from './Base';

const configuration = {
    apiUrl : properties.logsServiceUrl
}

const createUserLogs = (request) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(request),
    };

    return fetch(`${configuration.apiUrl}/api/userlog/`, requestOptions).then(handleResponse);
};

const UserLogsService = {
    createUserLogs,
};

export default UserLogsService;