import properties from '../conf/properties';
import handleResponse  from './Base';

const configuration = {
    apiUrl : properties.logsServiceUrl
}

const getAll = () => {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };

    return fetch(`${configuration.apiUrl}/api/topic/`, requestOptions).then(handleResponse);
};

const TopicService = {
    getAll,
};

export default TopicService;