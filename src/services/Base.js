function handleResponse(response) {

    console.log("response: ", response);

    return response.text().then(text => {

        const data = text && JSON.parse(text);
        console.log("response.ok: ", response.ok);
        
        if (!response.ok) {
            if (response.status === 401) {
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}

export default handleResponse;