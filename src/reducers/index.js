import { combineReducers } from "redux";
import {positions} from './positions';
import {topics} from './topics';


const rootReducer = combineReducers({
    positions : positions,
    topics : topics 
});

export default rootReducer;