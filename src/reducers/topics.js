import {
    RETRIEVE_TOPICS
} from '../actions/types';

const initialState = [];

export function topics(topics = initialState, action) {

    const { type, payload } = action;

    switch (type) {
        case RETRIEVE_TOPICS:
            return payload;

        default:
            return topics;
    }
};