import {
    RETRIEVE_POSITIONS
} from '../actions/types';

const initialState = [];

export function positions(positions = initialState, action) {

    const { type, payload } = action;

    switch (type) {
        case RETRIEVE_POSITIONS:
            return payload;

        default:
            return positions;
    }
};