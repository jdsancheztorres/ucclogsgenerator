import './App.css';
import Logs from './Logs';
import  {ThemeProvider}  from '@mui/material/styles';
import theme from '../src/theme';

function App() {
  return (
    <ThemeProvider theme={theme}>
    <div className="App">
      <Logs/>
    </div>
    </ThemeProvider>
  );
}

export default App;
